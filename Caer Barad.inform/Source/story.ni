"Caer Barad" by Silent Fox

Book 0 - Definitions

Chapter 0.1 - Story Metadata

The story headline is "An interactive adventure for Dungeons and Dragons 5th Edition."
The story genre is "Fantasy".
The release number is 1.
The story description is "To be done."
The story creation year is 2020.

Chapter 0.2 - Release

Release along with cover art ("Dark castle on a hill."), a website, a "Vorple" interpreter, the introductory booklet, an introductory postcard, the source text, and the library card.

Chapter 0.3 - Includes

Include Locksmith by Emily Short.
Include Basic Screen Effects by Emily Short.
Include Compass Rose by Rafael D'Airen.
Include Secret Doors by Andrew Owen.
Include Variable Time Control by Eric Eve.

Section 0.3.1 - Compass Rose

Table of Fancy Stati
left	central	right 
" [if in darkness]Darkness[otherwise][location][end if]"	""	"[top rose]" 
" [sec-time]"	""	"[middle rose]" 
" "	""	"[bottom rose]" 

Rule for constructing the status line:
	fill status bar with Table of Fancy Stati;
	say default letters;
	rule succeeds.

Chapter 0.4 - New Kinds

A stair is a kind of door. A stair is usually open and not openable.

An encounter is a kind of thing. An encounter is always fixed in place.

Chapter 0.4.1 - Items

An item is a kind of thing.
An item has some number called the Investigation DC. An item has some text called the Investigation Success. An item has some text called the Investigation Failure.

Report examining an item (this is the Investigation DC Reporting rule):
	if the Investigation DC of the noun is greater than zero:
		say "You may roll for Investigation with a DC [the Investigation DC].";
	the rule succeeds.

Investigating is an action applying to one touchable thing and requiring light.
Understand "investigate [something]" as investigating.

Chapter 0.5 - Introduction

When play begins:
	center "[Bold type]Welcome to Caer Barad[roman type][paragraph break]";
	say "Your path comes, at last, to a village that may have seen better days sometime long, long ago. The several weeks on the road leading to this moment were ones of scarcity and restlessness, your food reserves are nearing their end, and you are tired beyond compare.[paragraph break]As you reach the village's limits, the thick fog gives way to a large hill with a dilapidated castle on top of it.[paragraph break]Welcome to Caer Barad.";
	center "---[paragraph break]";
	say "[fixed letter spacing]You are now a second-level character in a fantasy setting to be discovered.[paragraph break]While this is an interact fiction, this also requires you to have a Dungeons & Dragons, 5th Edition, player sheet. Take this time to create one and have it ready for use.[variable letter spacing][paragraph break]";
	center "<Press any key>";
	wait for any key;
	clear the screen.

Book 1 - Adventure

Chapter 1.1 - Village Main

Village Entrance is a room. "A run-down port barely stands by the start of the village's main road.[paragraph break]For some meters north there are no buildings, and only then the village proper begins."
Entrance Port is a backdrop in the Village Entrance. "Several centuries-old blocks of hewn stone stand one over the other in a semblance of an archway. The blocks themselves seem to be close to the breaking point, and the structure may be only a storm away from giving in. Maybe do not delay under it, lest you taste the weight of it."
A skull is an item in the Village Entrance. "[If the Village Entrance is unvisited]You see an[else]The[end if] old piece of bone dettached from the rest of its body." The description of the skull is "Small, it must have belonged to a mouse or other rodent, and the fact it stands alone from the rest of its former body implies it must have been the victim of some predator that considered its head too unworthy of digestion." Understand "bone" as the skull. The Investigation DC of the skull is 10.

The Main Road 1 is north of the Village Entrance. "A few outlying buildings dot this section of the village's main road." The printed name of the Main Road 1 is "Main Road".
