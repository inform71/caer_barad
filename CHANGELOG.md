# [Caer Barad](https://gitlab.com/inform71/caer_barad) Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
### Added
- Code of Conduct from [Contributor Covenant](https://www.contributor-covenant.org/version/2/1/code_of_conduct/).
- [Creative Commons BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license.
- ReadMe file.
- [Changelog](https://keepachangelog.com/en/1.0.0/) file.
- Contributing file.
- ...

<!--
[0.0.1]: https://github.com/olivierlacan/keep-a-changelog/releases/tag/v0.0.1
-->
